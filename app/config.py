from fastapi import FastAPI
from pydantic import BaseSettings


class Settings(BaseSettings):
    OPENAPI_URL: str = "/openapi.json"
    SWAGGER_URL: str = "/docs"
    REDOC_URL: str = "/redoc"
    SECRET_KEY: str = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
    ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 30


settings = Settings()
